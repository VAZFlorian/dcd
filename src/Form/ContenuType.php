<?php

namespace App\Form;

use App\Entity\Contenu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ContenuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('image1', FileType::class, [
                'mapped' => false
            ])
            ->add('image2', FileType::class, [
                'mapped' => false
            ])
            ->add('image3', FileType::class, [
                'mapped' => false
            ])
            ->add('image4', FileType::class, [
                'mapped' => false
            ])
            ->add('image5', FileType::class, [
                'mapped' => false
            ])
            ->add('text1')
            ->add('text2')
            ->add('text3')
            ->add('text4')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contenu::class,
        ]);
    }
}
