<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Commentaires;
use App\Form\CommentairesType;
use App\Repository\CommentairesRepository;
use App\Entity\Reservations;
use App\Form\ReservationsType;
use App\Repository\ReservationsRepository;
use App\Entity\Produits;
use App\Form\ProduitsType;
use App\Repository\ProduitsRepository;


class EspaceAdminController extends AbstractController
{
    #[Route('/espace_admin', name: 'app_espace_admin')]
    public function index(CommentairesRepository $commentairesRepository, 
    ReservationsRepository $reservationsRepository, ProduitsRepository $produitsRepository): Response
    {

       
 
        return $this->render('espace_admin/index.html.twig', [
            'controller_name' => 'EspaceAdminController',
            'nbcommvis' => $commentairesRepository->countCommentsVisible()[0][1],
            'nbcommnotvis' => $commentairesRepository->countCommentsNotVisible()[0][1],
            'nbreserv' => $reservationsRepository->countReserv()[0][1],
            'nbproduitsdispo' => $produitsRepository->countProduitsDispo()[0][1],
            'nbproduitsindispo' => $produitsRepository->countProduitsIndispo()[0][1]
        ]);
    }
}
