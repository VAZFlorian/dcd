<?php

namespace App\Controller;

use App\Entity\Contenu;
use App\Form\ContenuType;
use App\Repository\ContenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\FileUploader;

#[Route('/contenu')]
class ContenuController extends AbstractController
{
    #[Route('/', name: 'app_contenu_index', methods: ['GET'])]
    public function index(ContenuRepository $contenuRepository): Response
    {
        
        return $this->render('contenu/index.html.twig', [
            'contenus' => $contenuRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_contenu_new', methods: ['GET', 'POST'])]
    public function new(Request $request,FileUploader $fileUploader, ContenuRepository $contenuRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $contenu = new Contenu();
        $form = $this->createForm(ContenuType::class, $contenu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

              
            $produitFile = $form->get('image1')->getData();
            if ($produitFile) {
                $produitFileName = $fileUploader->upload($produitFile);
                $contenu->setImage1($produitFileName);
        }

        $produitFile = $form->get('image2')->getData();
            if ($produitFile) {
                $produitFileName = $fileUploader->upload($produitFile);
                $contenu->setImage2($produitFileName);
        }

        $produitFile = $form->get('image3')->getData();
            if ($produitFile) {
                $produitFileName = $fileUploader->upload($produitFile);
                $contenu->setImage3($produitFileName);
        }

        $produitFile = $form->get('image4')->getData();
            if ($produitFile) {
                $produitFileName = $fileUploader->upload($produitFile);
                $contenu->setImage4($produitFileName);
        }

        $produitFile = $form->get('image5')->getData();
            if ($produitFile) {
                $produitFileName = $fileUploader->upload($produitFile);
                $contenu->setImage5($produitFileName);
        }

            $contenuRepository->add($contenu);
            return $this->redirectToRoute('app_contenu_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('contenu/new.html.twig', [
            'contenu' => $contenu,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_contenu_show', methods: ['GET'])]
    public function show(Contenu $contenu): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        return $this->render('contenu/show.html.twig', [
            'contenu' => $contenu,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_contenu_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request,FileUploader $fileUploader, Contenu $contenu, ContenuRepository $contenuRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $form = $this->createForm(ContenuType::class, $contenu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


               
            $produitFile = $form->get('image1')->getData();
            if ($produitFile) {
                $produitFileName = $fileUploader->upload($produitFile);
                $contenu->setImage1($produitFileName);
        }

        $produitFile = $form->get('image2')->getData();
            if ($produitFile) {
                $produitFileName = $fileUploader->upload($produitFile);
                $contenu->setImage2($produitFileName);
        }

        $produitFile = $form->get('image3')->getData();
            if ($produitFile) {
                $produitFileName = $fileUploader->upload($produitFile);
                $contenu->setImage3($produitFileName);
        }

        $produitFile = $form->get('image4')->getData();
            if ($produitFile) {
                $produitFileName = $fileUploader->upload($produitFile);
                $contenu->setImage4($produitFileName);
        }

        $produitFile = $form->get('image5')->getData();
            if ($produitFile) {
                $produitFileName = $fileUploader->upload($produitFile);
                $contenu->setImage5($produitFileName);
        }



            $contenuRepository->add($contenu);
            return $this->redirectToRoute('app_contenu_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('contenu/edit.html.twig', [
            'contenu' => $contenu,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_contenu_delete', methods: ['POST'])]
    public function delete(Request $request, Contenu $contenu, ContenuRepository $contenuRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        if ($this->isCsrfTokenValid('delete'.$contenu->getId(), $request->request->get('_token'))) {
            $contenuRepository->remove($contenu);
        }

        return $this->redirectToRoute('app_contenu_index', [], Response::HTTP_SEE_OTHER);
    }
}
